package com.task.manager;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.google.android.maps.*;
import com.task.manager.views.AddressOverlay;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/29/12
 * Time: 9:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddLocationMapActivity extends MapActivity {

    public static final String ADDRESS_RESULT = "address";

    private EditText addressText;
    private Button mapLocationButton;
    private Button useLocationButton;
    private MapView mapView;
    private Address address;
    private MyLocationOverlay myLocationOverlay;


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.add_location);
        setUpViews();
    }

    //onResume is use for continues the task, when application on work or when come back 2 application from anywhere
    @Override
    protected void onResume(){
        super.onResume();
        myLocationOverlay.enableMyLocation();
    }

    //onPause is use for give the commands when application goes to pause state
    @Override
    protected void onPause(){
        super.onPause();
        myLocationOverlay.disableMyLocation();
    }

    protected void mapCurrentAddress() {

        String addressString = addressText.getText().toString();
        Geocoder g = new Geocoder(this);
        List<Address> addressList;

        try {
            addressList = g.getFromLocationName(addressString, 1);
            if(addressList.size() > 0 && addressList != null){
                address = addressList.get(0);
                List<Overlay> mapOverlays = mapView.getOverlays();
                AddressOverlay addressOverlay = new AddressOverlay(address);
                mapOverlays.add(addressOverlay);
                mapOverlays.add(myLocationOverlay);
                mapView.invalidate();
                final MapController mapController = mapView.getController();
                mapController.animateTo(addressOverlay.getGeoPoint(), new Runnable() {
                    @Override
                    public void run() {
                        mapController.setZoom(12);
                    }
                });
                useLocationButton.setEnabled(true);
            } else {
                //Need to say that there are no location found
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setUpViews() {
        addressText = (EditText)findViewById(R.id.task_address);
        mapLocationButton = (Button)findViewById(R.id.map_location_button);
        useLocationButton = (Button)findViewById(R.id.use_this_location_button);
        mapView = (MapView)findViewById(R.id.map);
        myLocationOverlay = new MyLocationOverlay(this, mapView);
        mapView.getOverlays().add(myLocationOverlay);
        mapView.invalidate();

        useLocationButton.setEnabled(false);
        mapView.setBuiltInZoomControls(true);

        mapLocationButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mapCurrentAddress();
            }

        });

        useLocationButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if(address != null){
                    Intent intent = new Intent();
                    intent.putExtra(ADDRESS_RESULT, address);
                    setResult(RESULT_OK, intent);
                }
                finish();
            }
        });
    }

    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }

    @Override
    protected boolean isLocationDisplayed(){
        return true;
    }
}
