package com.task.manager.adapter;

import android.content.Context;
import android.location.Location;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.task.manager.R;
import com.task.manager.tasks.Task;
import com.task.manager.views.TaskListItem;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/19/12
 * Time: 9:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskListAdapter extends BaseAdapter {

    private ArrayList<Task> taskList;
    private Context context;
    private ArrayList<Task> filteredTasks;
    private ArrayList<Task> unfilterdTasks;

    public TaskListAdapter(ArrayList<Task> taskList, Context context) {
        super();
        this.taskList = taskList;
        this.unfilterdTasks = taskList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return taskList.size();
    }

    @Override
    public Task getItem(int i) {
        return (taskList == null) ? null : taskList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        TaskListItem taskListItem;
        if(view == null){
            taskListItem = (TaskListItem)View.inflate(context, R.layout.task_list_item, null);
        } else {
            taskListItem = (TaskListItem)view;
        }

        taskListItem.setTask(taskList.get(i));
        return taskListItem;
    }

    public void forceReload() {
        notifyDataSetChanged();
    }

    public void toggleTaskCompleteAtPosition(int position) {
        Task task = taskList.get(position);
        task.toggleComplete();
        notifyDataSetChanged();
    }

    public Long[] getCompleteTasksIds() {

        ArrayList<Task> completeTasks = new ArrayList<Task>();
        ArrayList<Long> completedIds = new ArrayList<Long>();
        for (Task task : taskList) {
            if(task.isComplete()){
                completeTasks.add(task);
                completedIds.add(task.getId());
            }
        }
        //remove completed tasks from the list
        taskList.removeAll(completeTasks);
        notifyDataSetChanged();
        return completedIds.toArray(new Long[]{});
    }

    public void filterTaskByLocation(Location latestLocation, long locationFilterDistance) {
        filteredTasks = new ArrayList<Task>();
        for (Task task : taskList) {
            if(task.hasLocation() && taskIsWithinGeofence(task, latestLocation, locationFilterDistance)){
                filteredTasks.add(task);
            }
        }
        taskList = filteredTasks;
        notifyDataSetChanged();
    }

    private boolean taskIsWithinGeofence(Task task, Location latestLocation, long locationFilterDistance) {
        float[] distanceArray = new float[1];
        Location.distanceBetween(
                task.getLatitude(),
                task.getLongitude(),
                latestLocation.getLatitude(),
                latestLocation.getLongitude(),
                distanceArray
        );
        return (distanceArray[0] < locationFilterDistance);
    }

    public void removeLocationFilter() {
        taskList = unfilterdTasks;
        notifyDataSetChanged();
    }
}
