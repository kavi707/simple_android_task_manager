package com.task.manager;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.task.manager.adapter.TaskListAdapter;
import com.task.manager.contacts.ViewContactsActivity;
import com.task.manager.tasks.Task;

public class ViewTaskActivity extends ListActivity implements LocationListener{

    private Button addContactButton;
    private Button addButton;
    private TaskManagerApplication app;
    private TaskListAdapter adapter;
    private Button removeButton;
    private AlertDialog noTaskCompleted;
    private AlertDialog noLocationDetect;
    private TextView locationText;
    private LocationManager locationManager;
    private Location latestLocation;
    private ToggleButton localTaskToggle;
    private static final long LOCATION_FILTER_DISTANCE = 20000; //distance from meter

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setUpViews();

        app = (TaskManagerApplication)getApplication();
        adapter = new TaskListAdapter(app.getCurrentTask(), this);
        setListAdapter(adapter);
        setUpLocation();
    }

    //onResume is use for continues the task, when application on work or when come back 2 application from anywhere
    //(keep doing this always)
    @Override
    protected void onResume() {
        super.onResume();
        locationManager.removeUpdates(this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60, 5, this);
        adapter.forceReload();
    }

    //onPause is use for give the commands when application goes to pause state (keep doing this always)
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        adapter.toggleTaskCompleteAtPosition(position);
        Task task = adapter.getItem(position);
        app.saveTask(task);
    }

    protected void removeCompleteTasks() {

        Long[] ids = adapter.getCompleteTasksIds();
        if(ids.length == 0){
            noTaskCompleted = new AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage("No completed Tasks to Remove")
                    .setNeutralButton(R.string.cancel, new AlertDialog.OnClickListener(){

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            noTaskCompleted.cancel();
                        }
                    }).create();
            noTaskCompleted.show();
        } else {
            app.removeTasks(ids);
        }

    }

    private void setUpViews() {

        addButton = (Button)findViewById(R.id.add_button);
        removeButton = (Button)findViewById(R.id.remove_button);
        locationText = (TextView)findViewById(R.id.current_location_text);
        localTaskToggle = (ToggleButton)findViewById(R.id.show_local_task_toggle);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ViewTaskActivity.this, AddTaskActivity.class);
                startActivity(intent);
            }
        });

        removeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                removeCompleteTasks();
            }
        });

        localTaskToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!showLocalTasks(localTaskToggle.isChecked())){
                    localTaskToggle.setChecked(false);
                }
            }
        });
    }

    public void viewExistingContactsButtonOnClick(View view){

        Intent intent = new Intent(ViewTaskActivity.this, ViewContactsActivity.class);
        startActivity(intent);
    }

    private void setUpLocation() {
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60, 5, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        this.latestLocation = location;
        String locationString = String.format("@ %f, %f +/- %fm",
                latestLocation.getLatitude(),
                latestLocation.getLongitude(),
                latestLocation.getAccuracy());
        locationText.setText(locationString);
    }

    protected boolean showLocalTasks(boolean checked) {

       boolean result = false;

       if(latestLocation != null){
           if(checked){
               adapter.filterTaskByLocation(latestLocation, LOCATION_FILTER_DISTANCE);
           } else {
               adapter.removeLocationFilter();
           }

           result = true;
       } else {
           noLocationDetect = new AlertDialog.Builder(this)
                   .setTitle(R.string.error)
                   .setMessage("No Location found. Please check you GPS is working")
                   .setNeutralButton(getString(R.string.ok_message), new AlertDialog.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialogInterface, int i) {
                           noLocationDetect.cancel();
                       }
                   }).create();
           result = false;
           noLocationDetect.show();
       }

        return result;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        //If our location provider is wifi or internet, then if we are not there service area
        //(that mean, current connection status is change) then gives a message to us
    }

    @Override
    public void onProviderEnabled(String s) {
        //Give a message when location provider(in our case - GPS) enabled
    }

    @Override
    public void onProviderDisabled(String s) {
        //Give a message when location provider(in our case - GPS) disabled
    }
}
