package com.task.manager;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.task.manager.tasks.Task;
import com.task.manager.tasks.TaskSQLiteOpenHelper;

import java.util.ArrayList;

import static com.task.manager.tasks.TaskSQLiteOpenHelper.*;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/16/12
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskManagerApplication extends Application {


    private ArrayList<Task> currentTask = null;
    private SQLiteDatabase database;


    @Override
    public void onCreate() {
        super.onCreate();
        TaskSQLiteOpenHelper helper = new TaskSQLiteOpenHelper(this);
        database = helper.getWritableDatabase();

        if(currentTask == null) {
            loadTask();
        }
    }

    public ArrayList<Task> getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(ArrayList<Task> currentTask) {
        this.currentTask = currentTask;
    }

    public void addTask(Task t){
        assert(t != null);

        ContentValues values = new ContentValues();
        values.put(TASK_NAME, t.getName());
        values.put(TASK_COMPLETE, Boolean.toString(t.isComplete()));
        values.put(TASK_ADDRESS, t.getAddress());
        values.put(TASK_LATITUDE, t.getLatitude());
        values.put(TASK_LONGITUDE, t.getLongitude());

        long id = database.insert(TABLE_NAME, null,values);
        t.setId(id);

        currentTask.add(t);
    }

    public void saveTask(Task t){

        assert(t != null);

        ContentValues values = new ContentValues();
        values.put(TASK_NAME, t.getName());
        values.put(TASK_COMPLETE, Boolean.toString(t.isComplete()));
        values.put(TASK_ADDRESS, t.getAddress());
        values.put(TASK_LATITUDE, t.getLatitude());
        values.put(TASK_LONGITUDE, t.getLongitude());

        long id = t.getId();
        String where = String.format("%s = ?",TASK_ID);
        database.update(TABLE_NAME,values,where, new String[]{id+""});

//        database.execSQL("update " + TABLE_NAME + " set " + TASK_COMPLETE + " = '" + Boolean.toString(t.isComplete()) + "' where " + TASK_ID + " = " + id);
    }

    public void removeTasks(Long[] ids){

        StringBuffer idList = new StringBuffer();
        for(int i=0; i < ids.length; i++) {
            idList.append(ids[i]);
            if(i < ids.length - 1)
                idList.append(",");
        }
        String where = String.format("%s in (%s)", TASK_ID, idList);
        database.delete(TABLE_NAME, where, null);
    }

    public void loadTask() {

        currentTask = new ArrayList<Task>();

        Cursor taskCursor = database.query(
                TABLE_NAME,
                new String[] {TASK_ID, TASK_NAME, TASK_COMPLETE, TASK_ADDRESS, TASK_LATITUDE, TASK_LONGITUDE},
                null, null, null, null, String.format("%s,%s",TASK_COMPLETE,TASK_NAME)
        );

//        Cursor taskCursor = database.rawQuery("select * from " + TABLE_NAME + " ORDER BY " + TASK_COMPLETE + " , " + TASK_NAME, null);

        taskCursor.moveToFirst();
        Task task;
        if(!taskCursor.isAfterLast()){

            do{
                long id = taskCursor.getLong(0);
                String name = taskCursor.getString(1);
                boolean complete = Boolean.parseBoolean(taskCursor.getString(2));
                String address = taskCursor.getString(3);
                float latitude = taskCursor.getFloat(4);
                float longitude = taskCursor.getFloat(5);
                task = new Task(name);
                task.setComplete(complete);
                task.setId(id);
                task.setAddress(address);
                task.setLatitude(latitude);
                task.setLongitude(longitude);
                currentTask.add(task);
            } while (taskCursor.moveToNext());
        }
        taskCursor.close();
    }
}
