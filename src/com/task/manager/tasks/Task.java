package com.task.manager.tasks;

import android.location.Address;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/16/12
 * Time: 5:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class Task {

    private String name;
    private boolean complete;
    private long id;
    private String address;
    private double latitude;
    private double longitude;

    public Task(String taskName) {
        name = taskName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return name;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public void toggleComplete() {
        complete = !complete;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public void setAddress(Address a) {

        if(a == null){
            address = null;
            latitude = longitude = 0;
        } else {
            int maxAddressLine = a.getMaxAddressLineIndex();
            StringBuffer sb = new StringBuffer("");
            for (int i=0; i<maxAddressLine; i++){
                sb.append(a.getAddressLine(i) + ", ");
            }
            this.address = sb.toString();
            this.latitude = a.getLatitude();
            this.longitude = a.getLongitude();
        }
    }

    public boolean hasAddress(){
        return null != address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean hasLocation() {
        return (latitude != 0 && longitude != 0);
    }
}
