package com.task.manager.tasks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/23/12
 * Time: 6:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskSQLiteOpenHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "task_db.sqlite";
    public static final int VERSION = 2;
    public static final String TABLE_NAME = "tasks";
    public static final String TASK_ID = "id";
    public static final String TASK_NAME = "name";
    public static final String TASK_COMPLETE = "complete";
    public static final String TASK_ADDRESS = "address";
    public static final String TASK_LATITUDE = "latitude";
    public static final String TASK_LONGITUDE = "longitude";

    public TaskSQLiteOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        if(VERSION == 2) {
            //in version 2, add new columns to the table - address, latitude, longitude
            sqLiteDatabase.execSQL("alter table " + TABLE_NAME + " add column " + TASK_ADDRESS + " text");
            sqLiteDatabase.execSQL("alter table " + TABLE_NAME + " add column " + TASK_LATITUDE + " real");
            sqLiteDatabase.execSQL("alter table " + TABLE_NAME + " add column " + TASK_LONGITUDE + " real");
        } else if(VERSION == 3) {
            //in version 3, change latitude and longitude types to real
            sqLiteDatabase.execSQL("alter table " + TABLE_NAME + " add column " + TASK_LATITUDE + " real");
            sqLiteDatabase.execSQL("alter table " + TABLE_NAME + " add column " + TASK_LONGITUDE + " real");
        } /*else if(VERSION == 4){
            //in version 3, change latitude and longitude types to float
            sqLiteDatabase.execSQL("alter table " + TABLE_NAME + " add column " + TASK_LATITUDE + " real");
            sqLiteDatabase.execSQL("alter table " + TABLE_NAME + " add column " + TASK_LONGITUDE + " real");
        }*/

    }

    private void createTable(SQLiteDatabase sqLiteDatabase) {

        String createTableQuery = "create table " + TABLE_NAME + " (" +
                TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                TASK_NAME + " text, " +
                TASK_COMPLETE + " text, " +
                TASK_ADDRESS + " text, " +
                TASK_LATITUDE + " real, " +
                TASK_LONGITUDE + " real " +
                ");";
        sqLiteDatabase.execSQL(createTableQuery);
    }


}
