package com.task.manager.views;

import android.view.View;
import com.task.manager.R;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.task.manager.tasks.Task;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/19/12
 * Time: 10:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskListItem extends LinearLayout {

    private CheckedTextView checkedTextView;
    private TextView addressText;
    private Task task;

    public TaskListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate(){
        super.onFinishInflate();
        checkedTextView = (CheckedTextView)findViewById(android.R.id.text1);
        addressText = (TextView)findViewById(R.id.address_text);
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
        checkedTextView.setText(task.getName());
        checkedTextView.setChecked(task.isComplete());
        if(task.hasAddress()){
            addressText.setText(task.getAddress());
            addressText.setVisibility(View.VISIBLE);
        } else {
            addressText.setVisibility(View.GONE);
        }
    }
}
