package com.task.manager;

import android.app.Activity;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/16/12
 * Time: 6:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TaskManagerActivity extends Activity {

    private TaskManagerActivity taskManagerActivity;

    protected TaskManagerApplication getTaskManagerApplication() {
        TaskManagerApplication taskManagerApplication = (TaskManagerApplication)getApplication();
        return taskManagerApplication;
    }

    public TaskManagerActivity getTaskManagerActivity(){
        return taskManagerActivity;
    }
}
