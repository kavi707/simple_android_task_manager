package com.task.manager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.task.manager.tasks.Task;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 9/16/12
 * Time: 9:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class AddTaskActivity extends TaskManagerActivity {

    private EditText addTaskEditView;
    private Button addButton;
    private Button cancelButton;
    private Boolean textChange = false;
    private AlertDialog unsavedChanges;

    private static final int REQUEST_CHOOSE_ADDRESS = 0;
    private Address address;
    private Button addLocationButton;
    private TextView addressText;

    private Button sendButton;
    private EditText editTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_task);

        setUpViews();
    }

    private void addTask() {

        String task = addTaskEditView.getText().toString();
        Task t = new Task(task);
        t.setAddress(address);
        getTaskManagerApplication().addTask(t);
        finish();
    }

    @Override
    protected void onResume(){

        super.onResume();
        if(address == null){
            addLocationButton.setVisibility(View.VISIBLE);
            addressText.setVisibility(View.GONE);
        } else {
            addLocationButton.setVisibility(View.GONE);
            addressText.setVisibility(View.VISIBLE);
            addressText.setText(address.getAddressLine(0));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if(REQUEST_CHOOSE_ADDRESS == requestCode && RESULT_OK == resultCode){
            address = data.getParcelableExtra(AddLocationMapActivity.ADDRESS_RESULT);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    //onClick for the Add Location to Task button. (another way to onClick listener)
    public void addLocationButtonClicked(View view){

        Intent intent = new Intent(AddTaskActivity.this, AddLocationMapActivity.class);
        startActivityForResult(intent, REQUEST_CHOOSE_ADDRESS);
    }

    protected void cancel() {

        if(textChange){

            unsavedChanges = new AlertDialog.Builder(this)
            .setTitle(R.string.unsaved)
            .setMessage(R.string.message)
            .setPositiveButton(R.string.add_task, new AlertDialog.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    addTask();
                }
            })
            .setNeutralButton(R.string.discard, new AlertDialog.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    finish();
                }
            })
            .setNegativeButton(R.string.cancel, new AlertDialog.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    unsavedChanges.cancel();
                }
            }).create();
            unsavedChanges.show();
        } else {
            finish();
        }
    }

    private void setUpViews() {

        addTaskEditView = (EditText)findViewById(R.id.addTaskEditText);
        addButton = (Button)findViewById(R.id.addButton);
        cancelButton = (Button)findViewById(R.id.cancelButton);
        addLocationButton = (Button)findViewById(R.id.add_location_button);
        addressText = (TextView)findViewById(R.id.address_text);
        //http req sending test
        sendButton = (Button)findViewById(R.id.send_button);
        editTextView = (EditText)findViewById(R.id.show_text);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTask();
            }

        });

        addButton.setEnabled(false);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });

        addTaskEditView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //not applicable to this case
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textChange = true;
                addButton.setEnabled(true);

                String temp = addTaskEditView.getText().toString();

                if(temp.equals("")||temp.equals(null)){
                    addButton.setEnabled(false);
                } else {
                    addButton.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //not applicable to this case
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sentHttpPost();
            }
        });
    }

    //http req sending method
    private void sentHttpPost() {

        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
        HttpResponse response;
        JSONObject jsonObject = new JSONObject();
        List<String> addresses = new ArrayList<String>();

        try {
            HttpPost post = new HttpPost("http://10.0.2.2:7000/sms/send");
            addresses.add("tel:94776351232");
            jsonObject.put("applicationId","APP_000001");
            jsonObject.put("password", "pass");
            jsonObject.put("message","Hi android");
            jsonObject.put("destinationAddresses",addresses);
            jsonObject.put("deliveryStatusRequest",0);

            StringEntity se = new StringEntity(jsonObject.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);

            if(response != null){
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();

                if(statusCode == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream in = entity.getContent();
                    BufferedReader bfr = new BufferedReader(new InputStreamReader(in));
                    String line;
                    StringBuilder builder = new StringBuilder("");
                    while ((line = bfr.readLine()) != null){
                        builder.append(line + "\n");
                    }
                    in.close();
                    String result = builder.toString();
                    Log.d("Tag","Success Response : " + result);
                    editTextView.setText(result);
                }

            }
        } catch (Exception e){
            Log.d("Tag","Error Response : " + e);
        }

        /*Thread t = new Thread(){
          public void run(){

              Looper.prepare();
              HttpClient client = new DefaultHttpClient();
              HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
              HttpResponse response;
              JSONObject jsonObject = new JSONObject();
              List<String> addresses = new ArrayList<String>();

              try {
                  HttpPost post = new HttpPost("http://10.0.2.2:7000/sms/send");
                  addresses.add("tel:94776351232");
                  jsonObject.put("applicationId","APP_000001");
                  jsonObject.put("password", "pass");
                  jsonObject.put("message","Hi android");
                  jsonObject.put("destinationAddresses",addresses);
                  jsonObject.put("deliveryStatusRequest",0);

                  StringEntity se = new StringEntity(jsonObject.toString());
                  se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                  post.setEntity(se);
                  response = client.execute(post);

                  if(response != null){
                      StatusLine statusLine = response.getStatusLine();
                      int statusCode = statusLine.getStatusCode();

                      if(statusCode == 200) {
                          HttpEntity entity = response.getEntity();
                          InputStream in = entity.getContent();
                          BufferedReader bfr = new BufferedReader(new InputStreamReader(in));
                          String line;
                          StringBuilder builder = new StringBuilder("");
                          while ((line = bfr.readLine()) != null){
                            builder.append(line + "\n");
                          }
                          in.close();
                          String result = builder.toString();
                          LOGGER.info("Success Response : " + result);

                      }

                  }
              } catch (Exception e){
                  LOGGER.info("Error Response : " + e);
              }
              Looper.loop();
          }
        };
        t.start();*/
    }

}
